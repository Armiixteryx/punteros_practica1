#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	char vector[10];
	int *puntero;
	int x=0;
	srand(time(NULL));

	for(;x<11;x++) {
		vector[x] = rand()%100;
	}

	for(x=0;x<10;x++) {
		puntero = &vector[x];
		printf("Vector en posicion %d.\nValor de memoria: %p\n\n",x+1,puntero);
		printf("Ingrese cualquier tecla para continuar");
		getchar();
	}

	printf("Valores del vector: \n\n");
	for(x=0;x<10;x++) {
		printf("Vector[%d] = %d\n",x+1,vector[x]);
	}
	
	return 0;
}