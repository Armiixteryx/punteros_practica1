[Semestre 1]

Este es un simple ejercicio para mostrar la dirección de memoria de un número que pertenece a un array.
Además, aprovecho el ejercicio para probar las funciones rand y srand.

El ejercicio es:
Dado un vector de 10 elementos ={1, 2, 3, 4, 4, 7, 8, 9, 5, 4}, escribir un programa en C  (haciendo uso de puntero) que muestre las direcciones de memoria de cada elemento del vector.

Obtenido del siguiente blog: https://mari-cor-algo01.blogspot.com/2011/06/ejercicios-resultos-con-punteros-en-c.html
